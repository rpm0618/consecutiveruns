"""
A simple solution to one of the pre-interview tasks for the Hackerati interview.
Takes as input a list of unordered integers, and returns the start indicies of
consecutive runs (either increasing or decreasing) of length three.

Ryan McVeety - January 6, 1015
"""

def getArrayFromUser():
	userList = raw_input("Enter a space seperated list of numbers:\n>")
	try:
		# Split each number out of the string
		return [int(x) for x in userList.split()]
	except Exception:
		# If something went wrong, then the user entered the list incorrectly
		print "Incorrectly formatted list. Makes sure that all entries are integers, and are seperated by spaces"
		return []

def findConsecutiveRuns(array):
	runs = []
	# We only have to check up to the last two elements, as they can't have a 
	# run of three
	for x in xrange(len(array) - 2):
		# Check for ascending run. EX: [1, 2, 3]:
		# 1 == (2 - 1) and 2 == (3 - 1)
		if (array[x] == array[x + 1] - 1) and (array[x + 1] == array[x + 2] - 1):
			runs.append(x)
		# Check for descending run. EX: [3, 2, 1]:
		# 3 == (2 + 1) and 2 == (1 + 1)
		elif (array[x] == array[x + 1] + 1) and (array[x + 1] == array[x + 2] + 1):
			runs.append(x)
	return runs

def main():
	array = getArrayFromUser()
	print findConsecutiveRuns(array)

if __name__ == '__main__':
	main()